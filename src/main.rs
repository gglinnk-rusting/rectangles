#![allow(unused)]
use std::fmt;

struct Rectangle {
    width: u32,
    height: u32,
}

impl fmt::Display for Rectangle {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} by {}", self.width, self.height)
    }
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }
    fn can_hold(&self, rect: &Rectangle) -> bool {
        self.width > rect.width && self.height > rect.height
    }
    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}

fn main() {
    let rect = Rectangle {
        width: 30,
        height: 50,
    };
    let smaller_rect = Rectangle {
        width: 20,
        height: 40,
    };
    let bigger_w_rectangle = Rectangle {
        width: 40,
        height: 10,
    };
    let bigger_h_rectangle = Rectangle {
        width: 20,
        height: 60,
    };
    let sq = Rectangle::square(30);

    println!(
        "The area of {} rectangle is {} square pixel.",
        rect,
        rect.area()
    );
    println!(
        "{} rectangle can hold {} rectangle : {}",
        rect,
        smaller_rect,
        rect.can_hold(&smaller_rect)
    );
    println!(
        "{} rectangle can hold {} rectangle : {}",
        rect,
        bigger_w_rectangle,
        rect.can_hold(&bigger_w_rectangle)
    );
    println!(
        "{} rectangle can hold {} rectangle : {}",
        rect,
        bigger_h_rectangle,
        rect.can_hold(&bigger_h_rectangle)
    );
}
